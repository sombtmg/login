import 'package:flutter/material.dart';
import 'package:flutter_app3/contents/drawer.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  final appTitle = 'New_project';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: appTitle,
      theme: ThemeData(
        textTheme: TextTheme(
          display1: TextStyle(color: Colors.blueAccent),
        ),
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget {
  final String title;

  MyHomePage({Key key, this.title}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.redAccent,
        appBar: new AppBar(
          backgroundColor: Colors.redAccent,
          title: Text(
            "JustNep",
            textAlign: TextAlign.right,
          ),
        ),
        drawer: Drawer(
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                child: Text(
                  "JustNep",
                  textAlign: TextAlign.justify,
                  textScaleFactor: 2.0,
                ),
                decoration: BoxDecoration(color: Colors.redAccent),
              ),
              ListTile(
                title: Text("first"),
                onTap: () {
                  Navigator.pop(context);
                },
              ),
              ListTile(
                title: Text("first"),
                onTap: () {
                  Navigator.pop(context);
                },
              )
            ],
          ),
        ),
        body: Drwer());
  }
}
