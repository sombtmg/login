class NotificationModel {
  final String name;
  final String message;
  final String time;
  final String avatar;

  NotificationModel({this.name, this.message, this.time, this.avatar});

  List<NotificationModel> Data = [
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    ),
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    ),
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    ),
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    ),
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    ),
    new NotificationModel(
      name:"Alex",
      message: "whats up",
      time: "6:30",
      avatar: "https://upload.wikimedia.org/wikipedia/commons/0/01/LinuxCon_Europe_Linus_Torvalds_03_%28cropped%29.jpg",
    )
  ];

}